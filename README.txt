Description:
------------
This module adds a alters the default Views paging to create an infinite/sliding pager
functionality to ajax-enabled Views like twitter.com or facebook.com, where 'More' pops
out *below* the existing content, rather than replacing it.


A webform can be a questionnaires, contact or request form. These can be used 
by visitor to make contact or to enable a more complex survey that the type 
polls enable. Submissions from a webform are saved in a database table and 
can optionaly also be mailed to an e-mail address upon submission.

It was developed with fundings from AF Indurstri AB (www.af.se), 
Open Source City (www.opensourcecity.org) and Karlstad University Library 
(www.bib.kau.se).
Also many thanks to the Dupal community for all bughunting.

Code Contributions
------------------
thebuckst0p
cerup

(if you feel left out please send me a message thru the contact form 
on http://drupal.org/user/423896/contact)

Other implementations:
----------------------
for Drupal 5, it looks like http://drupal.org/project/ajax_views has similar functionality
for Drupal 7 (Views3), there is an implementation by Centogram 
 @ http://www.centogram.com/projects/drupal-infinite-scroll-pager-plugin-views-30
Endless Page (http://drupal.org/project/endless_page) for D6 seems to be similar but has been abandoned for >1yr


Todo:
-----
For an up to date todo list look at the issue tracker at:
http://drupal.org/project/issues/views_infinite_pager

